﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vidly.DataAccess;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class NewRentalsController : ApiController
    {
        private ApplicationDbContext _context;

        public NewRentalsController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public IHttpActionResult CreateNewRental(NewRentalDto newRentalDto)
        {
            //if (newRentalDto.MoviesId.Count == 0)
            //    return BadRequest("No movies id have been given.");
            var customer = _context.Customers.Single(c => c.Id == newRentalDto.CustomerId);
            //if (customer == null)
            //    return BadRequest("Customer id is invalid.");

            var movies = _context.Movies
                .Where(m => newRentalDto.MoviesId.Contains(m.Id))
                .ToList();
            //if (movies.Count != newRentalDto.MoviesId.Count)
            //    return BadRequest("One or more movies id is invalid.");

            foreach (var movie in movies)
            {
                if (movie.NumberAvailable == 0)
                    return BadRequest("Movie is not available.");

                movie.NumberAvailable--;

                var rental = new Rental
                {
                    Customer = customer,
                    Movie = movie,
                    RentedDate = DateTime.Now
                };
                _context.Rentals.Add(rental);
            }
            _context.SaveChanges();
            return Ok();
        }
    }
}

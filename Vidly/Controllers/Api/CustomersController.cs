﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using AutoMapper;
using System.Web.Http;
using Vidly.Models;
using Vidly.Dtos;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Http.Description;
using System.Data.Entity.Infrastructure;
using Vidly.DataAccess;

namespace Vidly.Controllers.Api
{
    public class CustomersController : ApiController
    {
        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }

        //GET: api/customers
        [HttpGet]
        public async Task<IHttpActionResult> GetCustomers(string query = null)
        {
            IQueryable<Customer> customersQuery = _context.Customers.Include(c => c.MembershipType);

            if (!String.IsNullOrWhiteSpace(query))
            {
                customersQuery = customersQuery.Where(c => c.Name.Contains(query));
            }

            var customers = await customersQuery.ToListAsync();

            var customersDto = customers.Select(Mapper.Map<Customer, CustomerDto>);

            return Ok(customersDto);
        }

        //GET: api/customers/2
        [ResponseType(typeof(CustomerDto))]
        [HttpGet]
        public async Task<IHttpActionResult> GetCustomer(int id)
        {
            Customer customerInDb = await _context.Customers.FindAsync(id);
            if (customerInDb == null) return NotFound();
            return Ok(Mapper.Map<Customer, CustomerDto>(customerInDb));
        }

        //Post: api/customers
        [ResponseType(typeof(CustomerDto))]
        [HttpPost]
        public async Task<IHttpActionResult> CreateCustomer(CustomerDto customerDto)
        {
            if (!ModelState.IsValid) return BadRequest();

            Customer customer = Mapper.Map<CustomerDto, Customer>(customerDto);
            _context.Customers.Add(customer);
            await  _context.SaveChangesAsync();

            customerDto.Id = customer.Id;
            return CreatedAtRoute("DefaultApi", new { id = customerDto.Id} , customerDto);

        }

        //Put: api/customer/2
        [ResponseType(typeof(CustomerDto))]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateCustomer(int id , CustomerDto customerDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            if (id != customerDto.Id) return BadRequest();

            var customerInDb = await _context.Customers.FindAsync(id);

            if (customerInDb == null) return NotFound();

            Mapper.Map(customerDto, customerInDb);

            _context.Entry(customerInDb).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerExists(id))
                {
                    return NotFound();
                }
                throw;
            }
            
            return StatusCode(HttpStatusCode.Accepted);
        }

        //Delete: api/customers/2
        [ResponseType(typeof(void))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteCustomer(int id)
        {
            var customerInDb = await _context.Customers.FindAsync(id);

            if (customerInDb == null) return NotFound();

            _context.Customers.Remove(customerInDb);
            await _context.SaveChangesAsync();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }

            base.Dispose(disposing);
        }

        private bool CustomerExists(int id)
        {
            return _context.Customers.Count(c => c.Id == id) > 0;
        }
    }
}

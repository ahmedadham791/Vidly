﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Vidly.DataAccess;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class MoviesController : ApiController
    {
        private ApplicationDbContext _context;

        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: api/Movies
        [HttpGet]
        public async Task<IHttpActionResult> GetMovies(string query = null)
        {
            var moviesQuery = _context.Movies.Include(m => m.Genre).Where(m => m.NumberAvailable > 0);
            if (!String.IsNullOrWhiteSpace(query))
            {
                moviesQuery = moviesQuery.Where(m => m.Name.Contains(query));
            }

            var movies = await moviesQuery.ToListAsync();
            var movieDto = movies.Select(Mapper.Map<Movie, MovieDto>);
            return Ok(movieDto);
        }

        // GET: api/Movies/2
        [ResponseType(typeof(MovieDto))]
        [HttpGet]
        public async Task<IHttpActionResult> GetMovie(int id)
        {
            Movie movieInDb = await _context.Movies.FindAsync(id);
            if (movieInDb == null) return NotFound();
            return Ok(Mapper.Map<Movie , MovieDto>(movieInDb));
        }

        // PUT: api/Movies/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateMovie(int id, MovieDto movieDto)
        {
            if (!ModelState.IsValid)  return BadRequest(ModelState);
            
            if (id != movieDto.Id)
                return BadRequest();

            Movie movieInDb = await _context.Movies.FindAsync(id);

            Mapper.Map(movieDto, movieInDb);
            _context.Entry(movieInDb).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.Accepted);
        }

        // POST: api/Movies
        [ResponseType(typeof(MovieDto))]
        [HttpPost]
        public async Task<IHttpActionResult> CreateMovie(MovieDto movieDto)
        {
            if (!ModelState.IsValid)  return BadRequest(ModelState);

            Movie movie = Mapper.Map<MovieDto, Movie>(movieDto);
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            movieDto.Id = movie.Id;
            return CreatedAtRoute("DefaultApi", new { id = movie.Id }, movieDto);
        }

        // DELETE: api/Movies/5
        [ResponseType(typeof(void))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteMovie(int id)
        {
            Movie movie = await _context.Movies.FindAsync(id);
            if (movie == null) return NotFound();


            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
            
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Count(e => e.Id == id) > 0;
        }
    }
}
namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsersRoles : DbMigration
    {
        public override void Up()
        {
            Sql(@"INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'724e7987-63f4-47c9-9cfa-edb8b2ed2efe', N'adham@test.com', 0, N'AK0RiP4py+KkzkwwXI7imu04OJigbreAsnECHxAfV9BYKM4Cv4ral5+7JcrSm4ziJw==', N'46193e87-a176-480b-b6b2-fa9a61fa7dab', NULL, 0, 0, NULL, 1, 0, N'adham@test.com')
                  INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'062aef26-88ff-4536-8b91-acf20ef83e6d', N'CanManageMovies')
                  INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c090b746-1c54-4fa4-b252-f9f101c65f51', N'admin@vidly.com', 0, N'AN8I8slt6FNm477xeAnCQwf+fOF0+97VaOsE5ohnIkWCHjZSp/2LfVZS8HKf4g+gYw==', N'3204b43f-d84a-4d12-a4f3-ac0511187955', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
                  INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c090b746-1c54-4fa4-b252-f9f101c65f51', N'062aef26-88ff-4536-8b91-acf20ef83e6d')");
        }
        
        public override void Down()
        {
        }
    }
}

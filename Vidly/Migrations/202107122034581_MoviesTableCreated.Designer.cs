// <auto-generated />
namespace Vidly.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class MoviesTableCreated : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(MoviesTableCreated));
        
        string IMigrationMetadata.Id
        {
            get { return "202107122034581_MoviesTableCreated"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

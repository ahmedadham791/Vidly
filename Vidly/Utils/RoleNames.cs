﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vidly.Utils
{
    public static class RoleNames
    {
        public const string CanManageMovies = "CanManageMovies";
    }
}
﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            //Model ---> DTo
            Mapper.CreateMap<Customer, CustomerDto>();
            Mapper.CreateMap<Movie, MovieDto>();
            Mapper.CreateMap<MembershipType, MembershipTypeDto>();
            Mapper.CreateMap<Genre, GenreDto>();
            //DTo ---> Model
            Mapper.CreateMap<CustomerDto, Customer>()
                  .ForMember(c => c.Id , option => option.Ignore());
            Mapper.CreateMap<MovieDto, Movie>()
                  .ForMember(m => m.Id , option => option.Ignore());

           // var config = new MapperConfiguration(cfg => {
            //    cfg.AddProfile<MappingProfile>();
            //    cfg.CreateMap<Customer, CustomerDto>();
           // });
            //var mapper = config.CreateMapper();
        }

    }
}
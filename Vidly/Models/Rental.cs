﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Vidly.Models
{
    public class Rental
    {
        public int Id { get; set; }

        public DateTime RentedDate { get; set; }

        public DateTime? ReturnedDate { get; set; }

        [Required]
        public Movie Movie { get; set; }

        [Required]
        public Customer Customer { get; set; }
    }
}